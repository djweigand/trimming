\documentclass[aps, pra, preprint, amsmath,showkeys, showpacs, amssymb]{revtex4-1}

\usepackage{cleveref}
%\usepackage{graphicx}
\usepackage{xcolor}

\usepackage{amssymb} % mathcal
\usepackage{braket}

\color[rgb]{1,1,1}
\pagecolor[rgb]{0.13,0.13,0.13}
%\pagecolor[rgb]{0,0,0}

\newcommand{\eg}{e.\,g.\ }
\newcommand{\ie}{i.\,e.\ }
\newcommand{\etal}{\emph{et al.\ }}

\renewcommand{\i}{{\mathrm{i}}}
\newcommand{\e}{{\mathrm{e}}}

\begin{document}

\title{Trimming in GKP prep}
\author{Daniel J. Weigand}
\affiliation{QuTech, Delft University of Technology, Lorentzweg 1, 2628 CJ Delft, The Netherlands}
\date{\today}

\maketitle
\section{Conventions}
$[q,p]=\i$

$X,Y,Z$ refer to the logicals acting on the GKP state, $S_p, S_q$ are the stabilizers.
$\sigma_X,\sigma_Y,\sigma_Z$ refer to the ancilla qubit

``Phase estimation'' refers to any kind of circuit where we do a controlled displacement of the oscillator, followed by a measurement of the ancilla qubit.

\section{Why trimming}
\begin{figure}
    %\includegraphics{yale_sequence.png}
    \caption{Measurement sequence used in the yale paper \cite{Campagne-Ibarcq.etal.2019:Yalegkp}. Phase estimation is sandwiched by ``trimming'' rounds, which reduce the photon number.}
    \label{fig:yale}
\end{figure}

A fundamental problem is that the error rate of a GKP code increases significantly if the photon number becomes large.
The reason is that for large photon numbers, photon loss is no longer the dominant source of noise, and other, less favorable, error channels (e.g. Kerr) become more important.

So, in order to stabilize a GKP qubit with phase estimation, we want that the photon number converges to a constant.
Each round of phase estimation adds about $\pi/2$ photons to a GKP state ($(\sqrt{2\pi}/2)^2$, taking a balanced controlled displacement into account).
Therefore the condition for a stable code state is
\begin{align}
    \alpha^2 &= \e^{-\kappa t}\alpha^2 + \pi/2 \approx (1-\kappa t)\alpha^2 + \pi/2,\\
    \kappa t \alpha^2 &= \pi/2
\end{align}
Considering that $t\approx 2\mu s$, $\kappa \approx 1/(200 \mu s)$, we have $\alpha^2 \approx 50\pi \approx 150$, \ie a GKP state would stabilize once it reaches $\sim 150$ photons.
Thus, photon loss alone is not sufficient to obtain a stable code state (with low error rate).

\section{Action of trimming}
In the Yale experiment, they therefore used trimming to reduce the photon number, see \cref{fig:yale} \cite{Campagne-Ibarcq.etal.2019:Yalegkp}.

We first get the action of one measurement round (measuring $S_p$, followed by one round of trimming).
To this end, we used the trick from the ETH group, namely that we can use so called ``error correction without measurement'', and obtain a measurement operator that is independent of the measurement result.

We always start with a state close to the $+1$ eigenstate of the stabilizer.
Therefore, it is optimal to read out the qubit in the $\sigma_Y$ basis \cite{Duivenvoorden.etal.2017:Sensorstate,Campagne-Ibarcq.etal.2019:Yalegkp}.

The oscillator-qubit state prior to measurement is given by:
\begin{align}
    \sum_\pm (X^\dag +\e^{\pm\i\pi/2} X)\ket{\Psi}_S \ket{\pm}_Q = \sum_\pm \e^{\pm\i\pi/4}(\e^{\mp\i\pi/4}X^\dag +\e^{\pm\i\pi/4} X)\ket{\Psi}_S \ket{\pm}_Q
\end{align}
The operator acting on the oscillator after qubit measurement is therefore (up to a global phase)
\begin{align}
    \cos\left(\pm\pi/4 + \sqrt{\pi}p\right).
\end{align}

using the trick by ETH, we can get rid of the relative phase.
A second conditional displacement yields
\begin{align}
    &\sum_\pm D(\mp\i\sqrt{\pi/8})\e^{\pm\i\pi/4}(\e^{\mp\i\pi/4}X^\dag +\e^{\pm\i\pi/4} X)\ket{\Psi}_S \ket{\pm}_Q\\
    &=\sum_\pm \e^{\pm\i\pi/4}(X^\dag + X)D(\mp\i\sqrt{\pi/8})\ket{\Psi}_S \ket{\pm}_Q 
\end{align}
Now there are two options: Measure the qubit, or trace it out.

The measurement operator acting on the storage oscillator, given result measurement result $x\in 0,1$ for the qubit readout, is given by
\begin{align}
    M_x = X^\dag +\e^{\i(-\pi/2+\pi x)} X \propto
    \e^{-\i(-\pi/2+\pi x)/2}X^\dag + \e^{\i(-\pi/2+\pi x)/2}X
    = \cos\left(\pm\pi/4 + \sqrt{\pi}p\right)
\end{align}
where we dropped a global phase.


\section{Weak Phase estimation}
A simple way to address the problem mentioned above is to reduce the number of photons added per round of phase estimation.
The idea is to make the measurement itself weaker, \ie changing it in a way that one obtains less than one bit of information per measurement.
This can be achieved by initializing the ancilla qubit not into the $\ket{+}$ state, but into a state $\frac{1}{\mathcal{N}}(\ket{0}+c\ket{1}), 0<c\leq 1$. The results can be seen in \cref{fig:weak_pe}.
At first glance, this does not seem beneficial, because each round of PE is weaker, thus more rounds are required to achieve the same fidelity.
However, the phase estimation rounds can now be done at double the frequency, because trimming rounds are no longer necessary.


\begin{figure}
    %\includegraphics{weak_pe.png}
    \caption{Envelope of a GKP state generated by phase estimation, using the state $\frac{1}{\mathcal{N}}(\ket{0}+c\ket{1}), 0<c\leq 1$ as initial state of the ancilla qubit.  Blue: $c=0.01$, Orange: $c=0.1$, Green: $c=1$}
    \label{fig:weak_pe}
\end{figure}

\section{Changing the measured stabilizer}
Assume that we measure the eigenvalue of an altered ``stabilizer'', $\e^{\i2\sqrt{\pi}q+\i\epsilon p}$
The combined qubit-cavity state after a phase estimation round, before measurement is given by
\begin{align}
    (1\pm \e^{\i2\sqrt{\pi}q+\i\epsilon p})\ket{\Psi}_{T}\ket{\pm Z}_A\\
    (1\pm \e^{-\i\sqrt{\pi}\epsilon}\e^{\i2\sqrt{\pi}q}\e^{\i\epsilon p})\ket{\Psi}_{T}\ket{\pm Z}_A\\
\end{align}
The probability to obtain result $x\in [0,1]$, and therefore the operator acting on the oscillator is given by
\begin{align}
    (\e^{\i(\pi x+\sqrt{\pi}\epsilon)/2}\e^{-\i\sqrt{\pi}q}\e^{-\i\epsilon p/2}+ \e^{-\i(\pi x +\sqrt{\pi}\epsilon)/2}\e^{\i\sqrt{\pi}q}\e^{\i\epsilon p/2})^2\\
    \left(\cos((\pi x+\sqrt{\pi}\epsilon-2\sqrt{\pi}q)/2)\cos(\epsilon p/2)-\sin((\pi x+\sqrt{\pi}\epsilon-2\sqrt{\pi}q)/2)\sin(\epsilon p/2)\right)^2
\end{align}

\bibliography{../../Bibliography/bibliography}
\end{document}
