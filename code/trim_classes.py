from typing import List, Sequence, Tuple, Optional

import matplotlib.pyplot as plt          # For plotting
import numpy as np                       # For math
import qutip                             # for Quantum operations

import warnings

# EXPLAINING USED QUTIP FUNCTION
# displace(N, alpha)        N=number of levels in Hilbert space, alpha = complex displacement amplitude.
# destroy(N)                Lowering (destruction) operator with N = number of levels in Hilbert space
# Dag()                     Returns adjoint (dagger) of object.
# tidyup()                  Removes small elements from the quantum object.
# expect(oper, state)       Calculates the expectation value for operator(s) and state(s)
#   oper [qobj/array-like]  A single or a list or operators for expectation value.
#   state [qobj/array-like] A single or a list of quantum states or density matrices
#   return value [float/complex/array-like]     Expectation value. real if oper is Hermitian, complex otherwise.
# basis(N,0)                Creates fock state ket vector, similar to Fock()
# isket(state)              Bool Indicates if the state is a ket
# qeye(N)                   Creates identity operator with size N (Hilbert space)
# fock_dm(self.N, 0)        Fock density matrix
# isoper(state)             Bool Indicates if the quantum object represents an operator
# Options(nsteps=100000)    Change the built in parameters of the dynamics solvers
# Qobj                      The type of a quantum object

# Table of contents, Each of these classes has a discription and questions
# Setup: class GKP_base, class GKP_base_ket(GKP_base), class GKP_PL(GKP_base), GKP_NC(GKP_base_ket)
# Protocol: GKP_yale(GKP_base_ket), GKP_notrim(GKP_base_ket), GKP_ARPE(GKP_notrim_NC), GKP_RPE(GKP_base_ket):


class GKP_base:
    """
    DESCRIPTION
    Shared code for all protocols

    QUESTIONS
    What is the difference between pos_spacing and spacing, and why is pos_spacing 1/20?
    Why and when is a function a callback?
    What is the M list exactly?

    ANSWERS
    pos_spacing is a trick, you can use it to get the mean value of the wave function, if we assume that its support in q is mostly contained within pos_spacing.
    A callback is simply a placeholder a function that can be called after another function is done with its job.
    The dictionary M is a shortcut to access the different measurement operators, see line 241
    """

    N = None                            # Number of Hilbert spaces of qubit     # SIZE of Hilbert space (Harmonic Osc with up to N photons)
    spacing = np.sqrt(2 * np.pi)        # Define the spacing as square root of 2*Pi # this is the spacing of the GKP code
    pos_spacing = spacing / 20          # Width of ? Create part of spacing?
    M = {}                              # Create what array?

    def __init__(self) -> None:         # Function called at creation of new GKP_Base, with type none and input self
        assert hasattr(self, "N")       # AssertionError is raised if self has no defined number of hilbert spaces N        # this is used to make sure that set_const is called BEFORE a class instance is initialized
        self.results = []               # Create empty results
        self.char = []                  # Create empty results
        self.counter = 0                # Create counter

    @classmethod
    def set_const(cls, _N):
        """
            DESCRIPTION
            Sets constants for the base of the GKP

            QUESTIONS
            What is the difference between SP and POS_P, why is the complex displacement amplitude 1/20?
            What do the SP, SQ, Pos_P, Pos_Q matrices do exactly?
            Why do the matrices be in lists?, to distribute them easier?
            What is cls.n, multiplication of annihilation matrices?
            Is the state set to None as a reset?

            ANSWERS
            The factor 1/20 is chosen such that most of the wave function is in \pm 40 \sqrt{\pi}
            SP, SQ are the "stabilizers" of the GKP code
            Pos_P, Pos_Q are also displacement operators, used to measure the mean of the wave function
            The operators are in dicts to facilitate access
            cls.n is the photon number operator
            Some variables are initialized here, because python throws an error otherwise. This includes state, set_ops, set_ops_callback
        """

        cls.N = _N                                               # Set number N, hilbert spaces # more precisely the size of the Hilbert space, i.e. number of photons

        # Stabilizers
        cls.SP = qutip.displace(_N, cls.spacing).tidyup()        # SP Matrix
        cls.SQ = qutip.displace(_N, 1j * cls.spacing).tidyup()   # SQ Matrix

        # These are used to get the expectation value of the wavefunc in q/p
        cls.POS_P = qutip.displace(_N, cls.pos_spacing)          # Pos P Matrix
        cls.POS_Q = qutip.displace(_N, 1j * cls.pos_spacing)     # Pos Q Matrix

        # which operator is this, and what does it do?
        cls.n = qutip.destroy(_N).dag() * qutip.destroy(_N)       # Declare lowering operators multiplication  # This is the photon number operator, n = a^\dag a

        cls.CHAR_OPS = [cls.SP, cls.SQ, cls.n]                  # List of OPS Matrices
        cls.CHAR_OPS_POS = [cls.POS_P, cls.POS_Q]               # List of OPS POS Matrices, not tidied up
        cls.state = None                                        # Declare state

    @classmethod
    def _kraus(cls, spacing: float) -> List[qutip.Qobj]:
        """
            DESCRIPTION
            Returns kraus qutip matrix list
            From inputs class and spacing
            Return value has 2 complex unit value

            QUESTIONS
            Why need a kraus matrix list?

            ANSWERS:
            This is the backaction of a measurement of the qubit on the oscillator, depending on the result
        """

        measurement_op = [
            (qutip.displace(cls.N, -spacing / 2)                # Variable 1, sum of 2 displacements
             + 1j * qutip.displace(cls.N, spacing / 2)).unit(),
            (qutip.displace(cls.N, -spacing / 2)                # Variable 2, sum of 2 displacements
             - 1j * qutip.displace(cls.N, spacing / 2)).unit()
        ]
        return measurement_op

    @classmethod
    def _correction(cls, spacing: float) -> List[qutip.Qobj]:
        """
            DESCRIPTION
            Returns matrix list with two displace matrix elements
            Corrects with the float input: spacing

            QUESTIONS:
            What is corrected?
            Why does list have 2 matrices?

            ANSWERS:
            In some protocols, we apply a displacement to the oscillator, depending on the measurement result (For example the Yale protocol).
            There are two options because the result can be zero or one.
        """
        cor = [
            qutip.displace(cls.N, spacing),
            qutip.displace(cls.N, -spacing)
        ]
        return cor

    @classmethod
    def set_ops(cls) -> None:
        raise Exception("Class GKP_base should not be called directly")  # Raising error of kind 'exception'

    @classmethod
    def _set_op_callback(cls) -> None:      # Empty function
        pass

    def characterize(self, full: bool = False) -> Tuple[float]:
        """
            DESCRIPTION
            Return |<S_q>|, |<S_p>| (Stabilizer), <n> for a given state
            If input full == true, additionally returns syn_p,syn_q,p,q

            QUESTIONS
            This function calculates the stabilizers and number of photons?
            Why are the extra variables only returns if the state is full?
            When is the state full?
            What is syn_p and syn_q?
            What is Delta_p and Delta_q?

            Delta_p and Delta_q are set to a value twice, why is this?

            Is this a simulation of a measurement on the transmom?

            The return value looks different than -> Tuple[float], giving an error, can this be changed?

            ANSWERS:
            ``full'' refers to the characterization, if we want a full characterization (all values) or only the short one.
            From the expectation value of a stabilizer, we get two values: the effective squeezing and the mean shift. These correspond to the standard deviation and mean of the peaks within the GKP-wavefunction.
            This is not a simulation of the measurement, but a way to estimate the quality of the GKP state obtained after the measurement.

            I'll have a look at the bug, should be easy to fix.
        """

        S_p, S_q, n_ph = qutip.expect(self.CHAR_OPS, self.state)
        # Displacement matrix CHAR_OPS functions as operators
        Delta_p = np.sqrt(np.log(1 / np.abs(S_p) ** 2)) / GKP_base.spacing
        Delta_q = np.sqrt(np.log(1 / np.abs(S_q) ** 2)) / GKP_base.spacing

        if full:
            syn_p = np.sqrt(2) * np.angle(S_p) / GKP_base.spacing
            syn_q = np.sqrt(2) * np.angle(S_q) / GKP_base.spacing
            p, q = qutip.expect(self.CHAR_OPS_POS, self.state)  # Calculates p and q from expected value displacements
            p = np.sqrt(2) * np.angle(p) / GKP_base.pos_spacing
            q = np.sqrt(2) * np.angle(q) / GKP_base.pos_spacing
            return Delta_p, Delta_q, n_ph, syn_p, syn_q, p, q   # return Error, not same as -> Tuple[float]
        else:
            return Delta_p, Delta_q, n_ph                       # return Error, not same as -> Tuple[float]

    def P(self, operator: Sequence[qutip.Qobj]) -> np.array:
        """
            DESCRIPTION
            P calculates the probability of measuring a 1 or 0.
            Returns 2x the absolute value of the expected value of the state        # x ** 2 is x squared! multiplication is x * 2
            The input is the operator over which the expected value is calculated

            QUESTIONS
            P is the probability of 1 or 0?
            is p a list when there are multiple operators?
            Why is p devided by the sum of p?
            Where is the abs and ** 2 for?

            ANSWERS:
            P is the probability to obtain 0 or 1 in the measurement, yes.
            Yes, P is a list of two values, to obtain 0 or 1.
            Because P(x) = |\braket{x}|^2
        """

        p = np.abs(qutip.expect(operator, self.state)) ** 2  # Calculates
        return p / np.sum(p)
# END class GKP_base:


class GKP_base_ket(GKP_base): 
    """
        DESCRIPTION
        Shared code for protocols using pure states (kets)
    """

    def __init__(self, state: Optional[qutip.Qobj] = None) -> None:  # Function called at creation of new GKB_base_ket
        super().__init__()               # Useful for accessing inherited methods that have been overridden in a class
        if state is None:                # If there is no state, create one
            self.state = qutip.basis(self.N, 0)
        else:                            # If state is given
            assert qutip.isket(state)    # State should be a ket state
            self.state = state           # Make the state of object the input state

    def _update(self, op_key: str, char: bool) -> None:
        """
            DESCRIPTION
            Updates the state
            _update is called in update function of protocols

            QUESTIONS
            What are the M matrix and op_key?
            When is char true?

            ANSWERS
            The dict M is used to access different operators. op_key is some string that characterizes the measurement. For example, (q, T) to indicate the trimming step of the q-measurement.
            char is set by the user. It is true if we want to store the characterization for every round
        """
        op = self.M[op_key]
        idx = np.random.choice(2, p=self.P(op))             # idx = 1 or 0, depends on chance and p probabilities
        self.state = op[idx] * self.state                   # Changes state
        self.state = self.state.unit()                      # Normalize state
        if char:
            self.results.append(2 * idx - 1)                # Append -1 or 1 to result
            self.char.append(self.characterize(True))       # Append outputs from characterize function

    def _update_callback(self):     # What does the callback function do?
        pass
# END class GKP_base_ket(GKP_base):


class GKP_PL(GKP_base):
    """
        DESCRIPTION
        Shared code for protocols using mixed states (density matrices), currently only used for
        photon loss
    """
    kappa = 0.01

    def __init__(self, state: Optional[qutip.Qobj] = None) -> None:     # Makes a state
        super().__init__()  # Useful for accessing inherited methods that have been overridden in a class
        if state is None:
            self.state = qutip.fock_dm(self.N, 0)
        else:
            assert qutip.isoper(state)
            self.state = state

    @classmethod
    def _set_op_callback(cls) -> None:
        cls.Mdag = {key: [op.dag() for op in value] for key, value in cls.M.items()}

    @classmethod
    def set_const(cls, _N) -> None:
        super().set_const(_N)
        cls.options = qutip.Options(nsteps=100000)
        cls.H = (0 * qutip.qeye(cls.N)).tidyup()
        cls.c_ops = [np.sqrt(cls.kappa) * qutip.destroy(cls.N)]
        cls.tlist = [0, 1]

    def _update_callback(self):
        self.state = qutip.mesolve(self.H, self.state, self.tlist, self.c_ops, options=self.options).states[
            -1].unit().tidyup()

    def _update(self, op_key: str, char: bool) -> None:
        op = self.M[op_key]
        opdag = self.Mdag[op_key]
        p = self.P(op)
        idx = np.random.choice(2, p=p)
        self.state = op[idx] * self.state * opdag[idx]
        nrm = np.trace(self.state)  # normalization by qutip fails for some reason
        self.state /= nrm
        self.state = self.state.tidyup()
        if char:
            self.results.append(2 * idx - 1)
            self.char.append(self.characterize(True))
# END class GKP_PL(GKP_base)


class GKP_base_ME(GKP_base):
    """
        DESCRIPTION
        Shared code for protocols using mixed states (density matrices) for full master equation solvers
    """
    t_rot = []
    t_disp = []
    t_idle = []

    C = {}
    Cdag = {}
    H_idle = qutip.Qobj()
    H_rot = qutip.Qobj()
    Disp = {}
    DispDag = {}

    def __init__(self, state: Optional[qutip.Qobj] = None) -> None:     # Makes a state
        super().__init__()  # Useful for accessing inherited methods that have been overridden in a class
        assert state is None
        # Initialize the oscillator in vacuum, the ancilla qubit in ground state
        self.state = qutip.tensor(
            qutip.fock_dm(self.N, 0), qutip.fock_dm(2,0)
            )

    @classmethod
    def _correction(cls, spacing: float) -> List[qutip.Qobj]:
        """
            Correction applied to qubit and oscillator if the measurement result of the qubit is zero or one.
        """
        cor = [
            qutip.tensor(qutip.displace(cls.N, spacing), qutip.qeye(2)),
            qutip.tensor(qutip.displace(cls.N, -spacing), qutip.sigmax())
        ]
        return cor

    @classmethod
    def _generate_displacement(cls, alpha):
        return qutip.tensor(
                 qutip.displace(cls.N, alpha),
                 qutip.qeye(2)
                 )

    @classmethod
    def set_const(cls, _N) -> None:
        super().set_const(_N)
        cls.options = qutip.Options(nsteps=100000)

    def _evolve(self, H: qutip.Qobj, t: float) -> None:
        self.state = qutip.mesolve(H, self.state, [0, t], self.c_ops, options=self.options).states[
            -1].unit().tidyup()

    def _measure(self) -> int:
        p = self.P(self.M)
        idx = np.random.choice(2, p=p)
        self.state = self.M[idx] * self.state * self.M[idx]
        nrm = np.trace(self.state)  # normalization by qutip fails for some reason
        self.state /= nrm
        self.state = self.state.tidyup()
        self._evolve(self.H_idle, self.t_idle)
        return idx

    def _displace(self, disp_key, dag:bool=False):
        if dag:
            self.state = self.DispDag[disp_key] * self.state * self.Disp[disp_key]
        else:    
            self.state = self.Disp[disp_key] * self.state * self.DispDag[disp_key]


    def _update(self, op_key: str, char: bool) -> None:
        # Sequence of rotations and displacements implementing a controlled displacement
        warnings.warn("fill the gates in")
        self._displace((op_key[0], "gamma"), dag=True)
        self._displace((op_key[0], "gamma"), dag=False)
        self._evolve(self.H_rot, self.t_rot)

        result = self._measure()
        self.state = self.C[result] * self.state * self.Cdag[result]
        if char:
            self.results.append(2 * result - 1)
            self.char.append(self.characterize(True))

class GKP_NC(GKP_base_ket):
    """
        DESCRIPTION
        Override a protocol such that no correction is used after measurements (only for GKP base ket)
    """

    @classmethod
    def _correction(cls, spacing: float) -> List[qutip.Qobj]:
        cor = [
            qutip.qeye(cls.N),          # Overrides correction matrix to identity matrix
            qutip.qeye(cls.N)           # Overrides correction matrix to identity matrix
        ]
        return cor
# END class GKP_NC(GKP_base_ket):

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ END SETUP, START PROTOCOLS

# The functions of the protocols are roughly the same for different protocols
# The main functions are update and set_ops
# set_ops is used to setup the protocol
# update is called every round to update the qubit


class GKP_yale_ME(GKP_base_ME):
    """
        DESCRIPTION
        The yale protocol, ‘sharpen q - trim q - sharpen p - trim p’, using a master equation solver
    """
    warnings.warn("fill kappa, gamma and t_rot in")
    # photon loss rate
    kappa = 0.01
    # time for the evolution and idling
    gamma = 0
    t_rot = 1           # van Look paper
    t_idle = 1

    # Placeholders.
    warnings.warn("delete CONST -- CONST3 once they are no longer required")
    CONST = 0
    CONST2 = 0
    CONST3 = qutip.Qobj()

    @classmethod
    def set_ops(cls) -> None:
        cls.c_ops = [np.sqrt(cls.kappa) * qutip.destroy(cls.N)]
        cls.M = [
            qutip.tensor(qutip.qeye(cls.N), qutip.fock_dm(2, 0)),
            qutip.tensor(qutip.qeye(cls.N), qutip.fock_dm(2, 1))
            ]
        spacing_m = {
            ("measurement", "sharpen"): GKP_base.spacing,
            ("measurement", "trim"): GKP_base.spacing / 20,
            ("correction", "sharpen"): 0.2 / np.sqrt(2),
            ("correction", "trim"): GKP_base.spacing / 2,
            ("measurement", "gamma"): cls.gamma,
        }
        for trim in ["sharpen", "trim"]:
            cls.C[("q", trim)] = cls._correction(spacing_m[("correction", trim)])
            cls.C[("p", trim)] = cls._correction(-1j * spacing_m[("correction", trim)])
        cls.Cdag = {key: [op.dag() for op in value] for key, value in cls.C.items()}

        warnings.warn("fill CONST and CONST2 in")
        for trim in ["sharpen", "trim", "gamma"]:
            cls.Disp[("q", trim)] = cls._generate_displacement(
                     cls.CONST * spacing_m[("measurement", trim)]
                     )
            cls.Disp[("p", trim)] = cls._generate_displacement(
                     cls.CONST2 * spacing_m[("measurement", trim)]
                     )
        cls.DispDag = {key: value.dag() for key, value in cls.Disp.items()}

        cls.H_idle = qutip.tensor((0 * qutip.qeye(cls.N)),(0 * qutip.qeye(2))).tidyup()
        warnings.warn("fill CONST3 in")
        cls.H_rot = cls.CONST3


    def update(self, characterize=True) -> None:
        assert characterize in (True, False, "round")
        for quadrature in ['q', 'p']:
            for trim in ["sharpen", "trim"]:
                op_key = (quadrature, trim)
                self._update(op_key, characterize is True)
        if characterize == 'round':
            self.char.append(self.characterize(True))


class GKP_yale(GKP_base_ket):
    """
        DESCRIPTION
        The yale protocol, ‘sharpen q - trim q - sharpen p - trim p’
    """

    M = {}

    @classmethod
    def set_ops(cls) -> None:
        spacing_m = {
            ("measurement", "sharpen"): GKP_base.spacing,
            ("measurement", "trim"): GKP_base.spacing / 20,
            ("correction", "sharpen"): 0.2 / np.sqrt(2),
            ("correction", "trim"): GKP_base.spacing / 2,
        }
        for trim in ["sharpen", "trim"]:
            c = cls._correction(spacing_m[("correction", trim)])
            k = cls._kraus(1j * spacing_m[("measurement", trim)])
            cls.M[("q", trim)] = [(c[i] * k[i]).tidyup() for i in range(2)]

            c = cls._correction(-1j * spacing_m[("correction", trim)])
            k = cls._kraus(spacing_m[("measurement", trim)])
            cls.M[("p", trim)] = [(c[i] * k[i]).tidyup() for i in range(2)]

        cls._set_op_callback()

    def update(self, characterize=True) -> None:
        assert characterize in (True, False, "round")
        for quadrature in ['q', 'p']:
            for trim in ["sharpen", "trim"]:
                op_key = (quadrature, trim)
                self._update(op_key, characterize is True)
        self._update_callback()
        if characterize == 'round':
            self.char.append(self.characterize(True))
# END class GKP_yale(GKP_base_ket):


class GKP_notrim(GKP_base_ket):
    """
        DESCRIPTION
        The yale protocol without trimming, ‘sharpen q - sharpen p’
    """

    M = {}

    @classmethod
    def set_ops(cls) -> None:

        spacing_c = 0.2 / np.sqrt(2)
        spacing_m = {
            "q": 1j * GKP_base.spacing,
            "p": GKP_base.spacing
        }

        for quadrature in ["q", "p"]:
            cls.M[quadrature] = [(cls._correction(spacing_c)[i] * cls._kraus(spacing_m[quadrature])[i]).tidyup() for i
                                 in range(2)]
        cls._set_op_callback()

    def update(self, characterize=True) -> None:
        assert characterize in (True, False, "round")
        for quadrature in ['q', 'p']:
            self._update(
                quadrature,
                (characterize is True)
            )
        self._update_callback()
        if characterize == 'round':
            self.char.append(self.characterize(True))
# END class  GKP_notrim(GKP_base_ket)


class GKP_notrim_NC(GKP_notrim, GKP_NC):
    pass


class GKP_ARPE(GKP_notrim_NC):
    """
        DESCRIPTION
        Similar to the yale protocol without trimming, but the correction is chosen such that the
        probability of measuring 0 or 1 is equal. This is the code used in https://arxiv.org/abs/1603.02242v4
    """

    def _feedback(self, quadrature):
        stabilizer = {"q": 1, "p": 0}[quadrature]
        phase = {"q": -1, "p": 1j}[quadrature]
        correct = qutip.expect(self.CHAR_OPS[stabilizer], self.state)
        correct = phase * np.angle(correct) / (self.spacing * np.sqrt(2))
        self.state = qutip.displace(self.N, correct / np.sqrt(2)) * self.state

    def update(self, characterize=True) -> None:
        assert characterize in (True, False, "round")
        for quadrature in ['q', 'p']:
            self._update(
                quadrature,
                (characterize is True)
            )
            self._feedback(quadrature)
        self._update_callback()
        if characterize == 'round':
            self.char.append(self.characterize(True))


class GKP_RPE(GKP_base_ket):
    """
        Kitaevs phase estimation algorithm. ‘sharpen q, X - sharpen p, X - sharpen q,
        Y - sharpen p, Y’
        We alternate the measurement basis of the ancilla qubit between the X and Y bases
    """

    M = {}

    @classmethod
    def _kraus_c(cls, spacing: float) -> List[qutip.Qobj]:
        #  Returns kraus qutip matrix list from inputs class and spacing
        kraus = [
            (qutip.displace(cls.N, -spacing / 2)
             + qutip.displace(cls.N, spacing / 2)).unit().tidyup(),
            (qutip.displace(cls.N, -spacing / 2)
             - qutip.displace(cls.N, spacing / 2)).unit().tidyup()
        ]
        return kraus

    @classmethod
    def set_ops(cls) -> None:
        #  Set up the protocol of RPE
        spacing_m = {
            "q": 1j * GKP_base.spacing,
            "p": GKP_base.spacing
        }
        for quadrature in ["q", "p"]:
            cls.M[(quadrature, "s")] = cls._kraus(spacing_m[quadrature])
            cls.M[(quadrature, "c")] = cls._kraus_c(spacing_m[quadrature])
        cls._set_op_callback()

    def update(self, characterize=True) -> None:
        #  Updates the protocol of RPE
        assert characterize in (True, False, "round"), "Characterise is not set to correct value"
        for phase in ["s", "c"]:
            for quadrature in ['q', 'p']:
                op_key = (quadrature, phase)
                self._update(
                    op_key,
                    (characterize is True)
                )
        self._update_callback()
        if characterize == 'round':
            self.char.append(self.characterize(True))


def combine_classes(GKPClass1, GKPClass2, class_name, threshold):
    @classmethod
    def set_ops(cls) -> None:
        cls.M = {**GKPClass1.M, **GKPClass2.M}

    def update(self, characterize: bool = True):
        if self.counter < self.threshold:
            GKPClass1.update(self, characterize=characterize)
        else:
            GKPClass2.update(self, characterize=characterize)
        self.counter += 1

    cls_dict = {
        "threshold": threshold,
        "set_ops": set_ops,
        "update": update
    }

    cls = type(class_name, (GKPClass1, GKPClass2), cls_dict)
    return cls


#GKP_yale_newI04 = combine_classes(GKP_yale, GKP_newI04, "GKP_yale_newI04", 20)
#GKP_rpe_newI04 = combine_classes(GKP_RPE, GKP_newI04, "GKP_rpe_newI04", 2)


class GKP_yale_NC(GKP_yale, GKP_NC):
    pass

class GKP_new04(GKP_base_ket):
    M = {}
    eps = 0.04

    @classmethod
    def set_ops(cls) -> None:
        spacing_m = {
            ("measurement", "+"): GKP_base.spacing * (1 + cls.eps),
            ("measurement", "-"): GKP_base.spacing * (1 - cls.eps)
        }
        for sign in ["+", "-"]:
            cls.M[("q", sign)] = cls._kraus(1j * spacing_m[("measurement", sign)])
            cls.M[("p", sign)] = cls._kraus(spacing_m[("measurement", sign)])

        cls._set_op_callback()

    def update(self, characterize=True) -> None:
        assert characterize in (True, False, "round")
        for sign in ["+", "-"]:
            for quadrature in ['q', 'p']:
                op_key = (quadrature, sign)
                self._update(op_key, characterize is True)
        if characterize == 'round':
            self.char.append(self.characterize(True))
