"""
In the Yale GKP experiment, they showed that trimming is necessary
to get a stable code state arXiv:1907.12487.
We want to see whether this trimming step can be included in the 'phase estimation' step of the protocol.

What we would like to achieve is that |<S_q>|, |<S_p>| go to a constant after some number of rounds, with the mean photon number also constant
"""
#%%
# Setup
import numpy as np
import matplotlib.pyplot as plt
import qutip
N = 200

I = qutip.qeye(N)

Sp = qutip.displace(N, np.sqrt(2*np.pi))
Sq = qutip.displace(N, 1j*np.sqrt(2*np.pi))

Xdag = qutip.displace(N, -np.sqrt(np.pi/2))
Zdag = qutip.displace(N, -1j*np.sqrt(np.pi/2))

n = qutip.destroy(N).dag() * qutip.destroy(N)

def characterize(state):
    """Return |<S_q>|, |<S_p>|, <n> for a given state
    """
    Delta_p = qutip.expect(Sp, state)
    Delta_q = qutip.expect(Sq, state)
    n_photons = qutip.expect(n, state)
    return np.abs(Delta_p), np.abs(Delta_q), n_photons

def PE_round(state, stabs):
    Sp_eps, Sq_eps = stabs
    state = Xdag * ((I+Sp_eps) * state).unit()
    state = Zdag * ((I+Sq_eps) * state).unit()
    return state


# %%
"""Baseline: No trimming

Does not converge. 1.3 photons added per round
"""
print("No Trimming")
STABS = [Sp, Sq]

state = qutip.basis(N, 0)
j=0
res = []
res.append([j, characterize(state)])
for i in range(30):
    state = PE_round(state, STABS)
    j+=1
    res.append([j, characterize(state)])

print(np.array(res))

# %%
"""Variant 1: small displacement along orthogonal axis

Converges to |<S_q>|=0.85 |<S_p>|=0.85 <n>=10.3 after 12 rounds
"""
print("small displacement along orthogonal axis")

epsilon = 0.05

Sp_eps = qutip.displace(N, np.sqrt(2*np.pi)*(1+1j*epsilon))
Sq_eps = qutip.displace(N, np.sqrt(2*np.pi)*(epsilon+1j))
Sp_eps_m = qutip.displace(N, np.sqrt(2*np.pi)*(1-1j*epsilon))
Sq_eps_m = qutip.displace(N, np.sqrt(2*np.pi)*(-epsilon+1j))

STABS = [Sp_eps, Sq_eps]
STABS_M = [Sp_eps_m, Sq_eps_m]

state = qutip.basis(N, 0)
j=0
res = []
res.append([j, characterize(state)])
for i in range(20):
    for s in (STABS, STABS_M):
        state = PE_round(state, s)
        j+=1
        res.append([j, characterize(state)])

print(np.array(res))
# %%
"""Variant 2: small displacement along same axis (lengthen/shorten)

Converges to |<S_q>|=0.83 |<S_p>|=0.87 <n>=10.3 after 13 rounds
"""
print("small displacement along same axis")
epsilon = 0.05

Sp_eps = qutip.displace(N, np.sqrt(2*np.pi)*(1+epsilon))
Sq_eps = qutip.displace(N, np.sqrt(2*np.pi)*1j*(1+epsilon))
Sp_eps_m = qutip.displace(N, np.sqrt(2*np.pi)*(1-epsilon))
Sq_eps_m = qutip.displace(N, np.sqrt(2*np.pi)*1j*(1-epsilon))

STABS = [Sp_eps, Sq_eps]
STABS_M = [Sp_eps_m, Sq_eps_m]

res = []
res.append([j, characterize(state)])
for i in range(20):
    for s in (STABS, STABS_M):
        state = PE_round(state, s)
        j+=1
        res.append([j, characterize(state)])

print(np.array(res))
# %%
"""Variant 3: Constant increased Vector length

Does not converge. Note: Numerics break after ~100 photons (N=200)
"""
print("Constant small displacement along same axis")
epsilon = 0.05

Sp_eps = qutip.displace(N, np.sqrt(2*np.pi)*(1+epsilon))
Sq_eps = qutip.displace(N, np.sqrt(2*np.pi)*1j*(1+epsilon))

STABS = [Sp_eps, Sq_eps]
state = qutip.basis(N, 0)
j=0
res = []
res.append([j, characterize(state)])
for i in range(30):
    state = PE_round(state, STABS)
    j+=1
    res.append([j, characterize(state)])

print(np.array(res))

# %%
"""Variant 4: Unbalanced ancilla

Inconclusive. Probably does not converge, but diverges only slowly.
"""
print("Unbalanced ancilla")
c = 0.05

def PE_round_unbalanced(state, c, j, res):
    state = Xdag * ((I+c*Sp) * state).unit()
    state = Zdag * ((I+c*Sq) * state).unit()
    res.append([j, characterize(state)])
    j+=1
    state = Xdag * ((c*I+Sp) * state).unit()
    state = Zdag * ((c*I+Sq) * state).unit()
    res.append([j, characterize(state)])
    return state

state = qutip.basis(N, 0)
j=0
res = []
res.append([j, characterize(state)])
for i in range(100):
    state = PE_round_unbalanced(state, c, j, res)
    j+=2

print(np.array(res))
# %%
