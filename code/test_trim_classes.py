import pytest

# Setup
import numpy as np
from trim_classes import GKP_base_ME, GKP_yale_ME
import qutip

class TestGKP_base_ME():
    N = 10
    GKP_base_ME.set_const(N)
    def test_init(self):
        with pytest.raises(AssertionError):
            GKP_base_ME(qutip.fock_dm(5, 0))


class TestGKP_yale_ME():
    def test_init(self):
        # Test that init raises errors if we give a state
        N = 10
        GKP_yale_ME.set_const(N)
        with pytest.raises(AssertionError):
            GKP_yale_ME(qutip.fock_dm(5, 0))

    def test_P(self):
        # Test that the probabilities only depend on the qubit and are correct
        N = 10
        GKP_yale_ME.set_const(N)
        gkp = GKP_yale_ME()
        gkp.set_ops()

        for x in [0, 1]:
            for _ in range(5):
                gkp.state = qutip.tensor(
                    qutip.rand_dm(gkp.N),
                    qutip.fock_dm(2,x)
                    )
                assert np.array_equal(gkp.P(gkp.M), [1- x, x])
        for _ in range(5):
            gkp.state = qutip.tensor(
                qutip.rand_dm(gkp.N),
                0.5*(qutip.fock_dm(2,0) + qutip.fock_dm(2,1))
                )
            assert np.array_equal(gkp.P(gkp.M), [0.5, 0.5])
    
    def test_measurement(self):
        # Test that pre- and post-measurement state are equal if we initialize and immediately measure
        N = 10
        GKP_yale_ME.set_const(N)
        gkp = GKP_yale_ME()
        gkp.set_ops()
        gkp.c_ops = []

        for x in [0, 1]:
            for _ in range(5):
                state0 = qutip.tensor(
                    qutip.rand_dm(gkp.N),
                    qutip.fock_dm(2,x)
                    )
                gkp.state = state0
                idx = gkp._measure()

                assert idx == x
                assert np.allclose(gkp.state,state0)

