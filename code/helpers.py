#%%
# Setup
import concurrent.futures
from typing import Any, Callable, Iterable, Optional, Tuple, Union
from concurrent.futures import as_completed
from tqdm import tqdm


def parallel_map(function: Callable[[Any], Any], iterable: Iterable, executor:Optional[concurrent.futures.ProcessPoolExecutor]=None,
                 chunksize: int=1, max_workers: Optional[int]=None, sum_up:Union[bool, Iterable]=False):
    """Parallel version of map(function, iterable).
       Wrapper used to initialize the Pool, if it is not given

    Arguments:
        function {Callable[[Any], Any]} -- function to be mapped
        iterable {Iterable} -- iterable the function should be applied to

    Keyword Arguments:
        executor {Optional[concurrent.futures.ProcessPoolExecutor]} -- A ProcessPoolExecutor can be given for repeated use (default: {None})
        chunksize {int} -- Send items to the pool in chunks instead of one-by-one. Can speed up (default: {1})
        max_workers {Optional[int]} -- Limit the number of worker processes. Useful if memory is a concern. (default: All logical cores)
        sum_up {Union[bool, Iterable]} -- If true, the results of the map are summed up (default: {False}).
                                          If an Iterable of equal length as iterable is given, it will be used as weight
    """
    if not isinstance(sum_up, bool):
        assert len(iterable) == len(sum_up)

    if executor is None:
        with concurrent.futures.ProcessPoolExecutor(max_workers=max_workers) as executor:
            output = _parallel_map(function, iterable, executor, chunksize, sum_up)
    else:
        assert max_workers is None
        output = _parallel_map(function, iterable, executor, chunksize, sum_up)
    return output

def _parallel_map(function: Callable[[Any], Any], iterable: Iterable, executor:concurrent.futures.ProcessPoolExecutor, chunksize: int, sum_up: Union[bool, Iterable]):
    """Parallel version of map(function, iterable).

    Arguments:
        function {Callable[[Any], Any]} -- function to be mapped
        iterable {Iterable} -- iterable the function should be applied to

    Keyword Arguments:
        executor {concurrent.futures.ProcessPoolExecutor} -- A ProcessPoolExecutor can be given for repeated use
        chunksize {int} -- Send items to the pool in chunks instead of one-by-one. Can speed up
        sum_up {bool} -- If true, the results of the map are summed up
    """
    if chunksize == 1:
        output = []
        if sum_up is False:
            futures = [executor.submit(_enumerate_f, counter, item, function) for counter, item in enumerate(iterable)]
            for f in tqdm(as_completed(futures), total=len(iterable)):
                output.append(f.result())
            output = [s[1] for s in sorted(output, key=lambda x: x[0])]
        elif sum_up is True:
            futures = [executor.submit(function, item) for item in iterable]
            t = tqdm(total=len(iterable))
            generator = as_completed(futures)
            output = next(generator).result()
            t.update()
            for f in generator:
                temp = f.result()
                output += temp
                t.update()
        else:
            assert len(sum_up) == len(iterable)
            futures = [executor.submit(_enumerate_f, counter, item, function) for counter, item in enumerate(iterable)]

            t = tqdm(total=len(iterable))
            generator = as_completed(futures)
            idx, temp = next(generator).result()
            output = sum_up[idx] * temp
            t.update()
            for f in generator:
                idx, temp = f.result()
                output += sum_up[idx] * temp
                t.update()
    else:
        assert not sum_up
        output = list(executor.map(function, iterable, chunksize=chunksize))
    return output

def serial_map(function: Callable[[Any], Any], iterable: Iterable, sum_up: Union[bool, Iterable]):
    """version of map(function, iterable) with progress bar and summing up

    Arguments:
        function {Callable[[Any], Any]} -- function to be mapped
        iterable {Iterable} -- iterable the function should be applied to

    Keyword Arguments:
        sum_up {bool} -- If true, the results of the map are summed up
    """
    if sum_up is False:
        output = [function(item) for item in tqdm(iterable)]
    elif sum_up is True:
        def _map(function, iterable):
            for item in iterable:
                yield function(item)
        output = sum(tqdm(_map(function, iterable), total=len(iterable)))
    else:
        assert len(sum_up) == len(iterable)
        def _map(function, iterable, sum_up):
            for i, item in enumerate(iterable):
                yield sum_up[i] * function(item)
        output = sum(tqdm(_map(function, iterable, sum_up), total=len(iterable)))
    return output

def _enumerate_f(counter:int, item: Any, function: Callable) -> Tuple[int, Any]:
    """ Apply function to item, return with the label counter.
        Helper for _parallel_map to get ordered returns from an asynchronous map

    Arguments:
        counter {int} -- Label used to sort results
        item -- items in the iterable over which the map is performed
        function {Callable} -- function that is mapped
    """
    return (counter, function(item))
