#%%
# Setup
import concurrent.futures
import os
from typing import Tuple

import matplotlib.pyplot as plt
import numpy as np

from helpers import parallel_map, serial_map
from test_trim_classes import test_GKP_yale
from trim_classes import GKP_base, GKP_notrim, GKP_notrim_NC, GKP_yale, GKP_yale_NC, GKP_new04, GKP_RPE, GKP_ARPE


def sample(gkp_class, n_rounds: int=20)->Tuple[np.array]:
    gkp = gkp_class()
    for _ in range(n_rounds):
        gkp.update(characterize="round")

    #Dp, Dq, n_ph, Syn_p, Syn_q, pos_p, pos_q = zip(*gkp.out)
    return np.array(gkp.char)

def helper(args):
    return sample(*args)

def plot(data: np.array, name: str, filetype: str='png')->None:
    titles = [
        r"Eff Squeezing P",
        r"Eff Squeezing Q",
        r"Mean Eff Squeezing",
        r"Momentum",
        r"Position",
        r"$\alpha$",
        r"Photon number"
        ]

    y_labels = [
        r"$\Delta_p$",
        r"$\Delta_q$",
        r"$(\Delta_p+\Delta_q)/2)$",
        r"$\langle \hat{p} \rangle$",
        r"$\langle \hat{q} \rangle$",
        r"$\sqrt{|\langle \hat{q} \rangle|^2+|\langle \hat{q} \rangle|^2}$",
        r"$\langle n_ph \rangle$"
        ]

    fig, axs = plt.subplots(2, 7, figsize=(7*5, 2*5))

    Delta_p = data[:, :, 0]
    Delta_q = data[:, :, 1]
    Delta_pq = (data[:, :, 0] + data[:, :, 1])/2
    pos_p = data[:, :, 5] / (2*np.sqrt(np.pi))
    pos_q = data[:, :, 6] / (2*np.sqrt(np.pi))
    alpha = np.sqrt(data[:, :, 5]**2 + data[:, :, 6]**2)
    n_ph = data[:, :, 2]

    x = np.arange(n_ph.shape[1])
    x = np.repeat([x], n_ph.shape[0], axis=0)

    idx_dat = zip(range(7), [Delta_p, Delta_q, Delta_pq, pos_p, pos_q, alpha, n_ph])

    for idx, dat in idx_dat:
        axs[0, idx].boxplot(dat, whis=[5, 95])
        axs[1, idx].scatter(x, dat, alpha=0.1, s=20)
        axs[0, idx].set_title(titles[idx])
        axs[0, idx].set(ylabel=y_labels[idx])
        axs[0, idx].set(ylabel=y_labels[idx])

    max_p = np.max(pos_p)
    max_q = np.max(pos_q)
    lim_pq_max = np.min([max_p, max_q])

    min_p = -np.min(pos_p)
    min_q = -np.min(pos_q)
    lim_pq_min = np.min([min_p, min_q])

    pos_pq_lim = np.min([lim_pq_max, lim_pq_min, 1.5])
    #pos_pq_lim = np.min([lim_pq_max, lim_pq_min])

    #axs[0, 0].set_ylim([0, 0.5])
    #axs[1, 0].set_ylim([0, 0.5])
    #axs[0, 1].set_ylim([0, 0.5])
    #axs[1, 1].set_ylim([0, 0.5])

    axs[0, 3].set_ylim([-pos_pq_lim, pos_pq_lim])
    axs[1, 3].set_ylim([-pos_pq_lim, pos_pq_lim])
    axs[0, 4].set_ylim([-pos_pq_lim, pos_pq_lim])
    axs[1, 4].set_ylim([-pos_pq_lim, pos_pq_lim])

    axs[0, 5].set_ylim([0, np.sqrt(2) * pos_pq_lim])
    axs[1, 6].set_ylim([0, np.sqrt(2) * pos_pq_lim])

    axs[0, 6].set_ylim([0, np.max(n_ph)])
    axs[1, 6].set_ylim([0, np.max(n_ph)])

    #ax.set_xlabel(r'$\bar{n}_A$')
    #ax.set_xticks(np.arange(0, 40, 10))
    fig.tight_layout()
    fig.savefig(f"figures/samples_{name}.{filetype}", bbox_inches='tight', pad_inches=0)
    plt.close(fig)

# This needs to be globally set for parallelization
gkp_classes = {
    "test_GKP_yale": test_GKP_yale,
    "GKP_notrim": GKP_notrim,
    "GKP_notrim_NC": GKP_notrim_NC,
    "GKP_yale": GKP_yale,
    "GKP_yale_NC": GKP_yale_NC,
    "GKP_new04": GKP_new04,
    "GKP_RPE": GKP_RPE,
    "GKP_ARPE": GKP_ARPE,
    }
#name_list = ["GKP_newI02", "GKP_newI04", "GKP_newI06", "GKP_yale", "GKP_RPE", "GKP_rpe_newI04", "GKP_yale_newI04", "GKP_yale_NC", "GKP_yale_NC2", "GKP_notrim", "GKP_notrim_NC"]
#name_list = ["GKP_yale_NC2", "GKP_notrim", "GKP_notrim_NC"]
#name_list = ["GKP_ARPE"]
#name_list = ["GKP_rpe_newI04", "GKP_yale_newI04"]

#prereq = ["GKP_yale", "GKP_newI04", "GKP_RPE"]
prereq = ["GKP_yale", "GKP_RPE"]





# +++++++++++++++++++++++++++++++++++++++++++++++++++++
# +++++++++++++++++++++++++++++++++++++++++++++++++++++
# CHANGE ONLY THIS
name_list = ["GKP_ARPE"]
N = 300
NR_SAMPLES = 300
NR_ROUNDS = 40

parallel = True
# +++++++++++++++++++++++++++++++++++++++++++++++++++++
# +++++++++++++++++++++++++++++++++++++++++++++++++++++





class_list_prereq = [gkp_classes[name] for name in prereq]
class_list = [gkp_classes[name] for name in name_list]

GKP_base.set_const(N)
# GKP_PL.set_const(N)

for gkp_cls in class_list_prereq:
    gkp_cls.set_ops()

for gkp_cls in class_list:
    if gkp_cls not in class_list_prereq:
        gkp_cls.set_ops()

# %%
if __name__ == "__main__":
    print(os.getcwd())
    if parallel:
        EXECUTOR = concurrent.futures.ProcessPoolExecutor()

    #for NR_ROUNDS in [10, 20, 30, 40]:
    for name in name_list:
        print(name)
        gkp_class = gkp_classes[name]

        arguments = [[gkp_class, NR_ROUNDS]] * NR_SAMPLES

        # shape:NR_SAMPLES, NR_ROUNDS, 7
        # Third axis: Dp, Dq, n_ph, Syn_p, Syn_q, pos_p, pos_q
        if parallel:
            out = np.array(parallel_map(helper, arguments, executor=EXECUTOR))
        else:
            out = np.array(serial_map(helper, arguments, sum_up=False))
        out = np.real(out)

        plot(out, name)

        frac_8 = (np.sum(out[:, -2, 0] > 0.8) + np.sum(out[:, -2, 1] > 0.8) + np.sum(out[:, -1, 0] > 0.8) + np.sum(out[:, -1, 1] > 0.8)) / (4 * NR_SAMPLES)
        frac_75 = (np.sum(out[:, -2, 0] > 0.75) + np.sum(out[:, -2, 1] > 0.75) + np.sum(out[:, -1, 0] > 0.75) + np.sum(out[:, -1, 1] > 0.75)) / (4 * NR_SAMPLES)
        frac_7 = (np.sum(out[:, -2, 0] > 0.7) + np.sum(out[:, -2, 1] > 0.7) + np.sum(out[:, -1, 0] > 0.7) + np.sum(out[:, -1, 1] > 0.7)) / (4 * NR_SAMPLES)

        print(frac_8)
        print(frac_75)
        print(frac_7)
