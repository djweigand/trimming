# STATE:
# Runs one experiment of trim_classes

# import concurrent.futures
# from helpers import parallel_map
import os
from test_trim_classes import test_GKP_yale
from wigner3d import plot_wigner

from trim_classes import GKP_base, GKP_notrim, GKP_notrim_NC, GKP_yale, GKP_yale_NC, GKP_new04, GKP_RPE, GKP_ARPE

from helpers import parallel_map


# This needs to be globally set for parallelization
gkp_classes = {
    "test_GKP_yale": test_GKP_yale,
    "GKP_notrim": GKP_notrim,
    "GKP_notrim_NC": GKP_notrim_NC,
    "GKP_yale": GKP_yale,
    "GKP_yale_NC": GKP_yale_NC,
    "GKP_new04": GKP_new04,
    "GKP_RPE": GKP_RPE,
    "GKP_ARPE": GKP_ARPE,
    }

# name_list = ["GKP_new04", "GKP_newI02", "GKP_newI04", "GKP_newI06", "GKP_yale", "GKP_RPE", "GKP_rpe_newI04",
#               "GKP_yale_newI04", "GKP_yale_NC", "GKP_yale_NC2", "GKP_notrim", "GKP_notrim_NC"]
# name_list = ["GKP_ARPE"]
# name_list = ["GKP_rpe_newI04", "GKP_yale_newI04"]

# Prereq are the prerequired classes
# prereq = ["GKP_yale", "GKP_newI04", "GKP_RPE"]
prereq = ["GKP_yale", "GKP_RPE"]

# +++++++++++++++++++++++++++++++++++++++++++++++++++++
# CHANGE ONLY THIS
name_list = ["GKP_RPE"]        # List of simulating protocols
N = 30                          # Number of Hilbert spaces (infinite ideal, but calculating time increases by ... )
NR_ROUNDS = 12                  # Number of update functions called

# +++++++++++++++++++++++++++++++++++++++++++++++++++++

# Creates lists of classes, both prerequired and used
class_list_prereq = [gkp_classes[name] for name in prereq]
class_list = [gkp_classes[name] for name in name_list]

FIGNAME_PREFIX = "figures/"

def plot_helper(args):
    """
        Helps make plot with input state and filename
        As output a wigner plot from plot_wigner code
    """
    state, figname = args
    plot_wigner(state, figname="figures/"+figname, lim=4)


# Runs if this state script is run directly
if __name__ == "__main__":
    state_list = []         # List that stores the states

    GKP_base.set_const(N)    # Set the constants of the Hilbert space
    # GKP_PL.set_const(N)

    for gkp_cls in class_list_prereq:           # Set up prereq classes in trim_classes
        gkp_cls.set_ops()

    for gkp_cls in class_list:                  # Set up classes in trim_classes
        if gkp_cls not in class_list_prereq:
            gkp_cls.set_ops()

    print(os.getcwd())          # Prints Map location

    for name in name_list:                  # For every protocol name in name_list
        print(name)                         # Prints name of protocol
        gkp_class = gkp_classes[name]       # Collect class from the name

        gkp = gkp_class()                   # Run class

        for _ in range(NR_ROUNDS):              # Update state preset number of rounds
            gkp.update(characterize="round")    # Update the state characterize is round for first round
        state_list.append(gkp.state)
        print(gkp.char[-1])
    arguments = list(zip(state_list, name_list))
    parallel_map(plot_helper, arguments)
