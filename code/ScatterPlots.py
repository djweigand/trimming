import numpy as np
from matplotlib import pyplot as plt
import matplotlib

import pandas as pd
import seaborn as sn

data = {"x": np.arange(100),
    'Retardation': np.random.random(100)
       }

df = pd.DataFrame(data)
df.corr()

# nr_bins=10
# bins = numpy.linspace(0, 1, nr_bins)
# digitized = numpy.digitize(ret, bins)
# bin_std = numpy.array([peakdist[digitized == i].std() for i in range(nr_bins)])
# print(peakdist[digitized == 0])
# #bin_std = numpy.array([diff[digitized == i].std() for i in range(nr_bins)])
# #print(diff[digitized == 0])
# print(bin_std)
# plt.plot(bin_std)
# plt.show()

sn.heatmap(df.corr(), annot=True)
plt.title('Correlation matrix roi mask s0090')

df.plot.hexbin(x='x', y='Retardation', colormap='viridis', gridsize=50, sharex=False, norm=matplotlib.colors.LogNorm())
#plt.ylim(0,5)
#plt.savefig("78_Rat_peakprominence_vs_Retardation", dpi=300)
plt.show()
